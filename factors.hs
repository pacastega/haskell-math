import Data.List

-- list of (prime, exponent)
factorsNaive :: Int -> [(Int, Int)]
factorsNaive n = tidy $ factors ++ forgottenPrime where
    factors = testCandidates n [2..intSqrt n]
    forgottenPrime = let prod = product factors in
      if prod == n then [] else [div n prod]

tidy :: Eq a => [a] -> [(a, Int)]
tidy = map (\list -> (head list, length list)) . group

testCandidates :: Int -> [Int] -> [Int]
testCandidates 1 _             = []
testCandidates n []            = []
testCandidates n cand@(c:cs) = if n `mod` c == 0
                             then c : testCandidates (n `div` c) cand
                             else testCandidates n cs
intSqrt :: Int -> Int
intSqrt n = intSqrtAux n 0 (n+1) where
    intSqrtAux n l r = if l >= r-1 then l else
      let m = div (l+r) 2 in
        if m*m <= n then intSqrtAux n m r else intSqrtAux n l m

prettyPrint :: Show a => [(a, Int)] -> String
prettyPrint =
  concat .
  intersperse " * " .
  map (\(a,b) -> (show a) ++ if b == 1 then "" else "^" ++ (show b))

-- Optimization: check the first primes and then discard all integers in
-- [2..intSqrt n] that are divisible by any of them

-- list of (prime, exponent)
factors :: Int -> [(Int, Int)]
factors n = tidy $ firstFactors ++ lastFactors ++ forgottenPrime
  where
  filteredPrimes = [2, 3, 5, 7]
  m = product filteredPrimes

  firstFactors = testCandidates n filteredPrimes
  n' = div n (product firstFactors)

  candidateRemainders = [rem | rem <- [1..m-1], gcd rem m == 1]
  candidateFactors = takeWhile (<= intSqrt n') $
    tail [k + m | k <- [0,m..], m <- candidateRemainders]

  lastFactors = testCandidates n' candidateFactors

  forgottenPrime = let prod = product lastFactors in
    if prod == n' then [] else [div n' prod]
