# Haskell Math
A collection of small programs written in Haskell to do fun mathematical
computations (mostly in Number Theory).
