import Data.List
import Data.Ratio

-- Calculate the coefficients in the continued fraction
continuedFraction :: (RealFrac a, Integral b) => a -> [b]
continuedFraction = unfoldr recipProperFraction where
  recipProperFraction x = let (int, frac) = properFraction x in
    if frac /= 0 then Just (int, recip frac) else Nothing

-- Simpify the continued fraction
foldContinuedFraction :: Integral b => [b] -> Ratio b
foldContinuedFraction = foldr1 collapse . map fromIntegral where
  collapse a b = a + (recip b)

-- Approximate a number by truncating its continued fraction and simplifying
rationalApproximation :: (RealFrac a, Integral b) => a -> Int -> Ratio b
rationalApproximation x n = foldContinuedFraction $ take n $ continuedFraction x

-- First ‘n’ rational approximations of ‘x’
main = do
  x <- putStr "Enter the number to be approximated: " *> readLn :: IO Double
  n <- putStr "Enter how many approximations you want: " *> readLn
  mapM_ (putStrLn . prettyPrint . rationalApproximation x) [1..n]
  where
    prettyPrint x = (show x ++ " ≈ " ++ show (fromRational x))
